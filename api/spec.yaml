openapi: 3.0.0
info:
  version: '1'
  title: License Manager
  description: 'License Manager API'

paths:
  /license:
    post:
      description: Adds a new license
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/newLicense"
      responses:
        '201':
          $ref: "#/components/responses/license"
        '401':
          $ref: "#/components/responses/error"
        '403':
          $ref: "#/components/responses/error"
        '500':
          $ref: "#/components/responses/error"
      security:
        - ApiKeyAuth: [admin]
  /license/{key}:
    parameters:
      - $ref: '#/components/parameters/licenseKey'
    get:
      description: Gets a license
      responses:
        '200':
          $ref: "#/components/responses/license"
        '401':
          $ref: "#/components/responses/error"
        '403':
          $ref: "#/components/responses/error"
        '500':
          $ref: "#/components/responses/error"
      security:
        - ApiKeyAuth: []
  /license/{key}/product/{name}:
    parameters:
      - $ref: '#/components/parameters/licenseKey'
      - $ref: '#/components/parameters/productName'
    get:
      description: Gets a product details
      responses:
        '200':
          $ref: "#/components/responses/product"
        '401':
          $ref: "#/components/responses/error"
        '403':
          $ref: "#/components/responses/error"
        '500':
          $ref: "#/components/responses/error"
      security:
        - ApiKeyAuth: []

components:
  schemas:
    newLicense:
      type: object
      properties:
        user:
          type: string
          example: 'jsmith'
          description: 'The user name the license belongs to'
        product:
          type: string
          example: 'my-theme'
          description: 'The product the key is for'

    license:
      type: object
      properties:
        user:
          type: string
          example: 'jsmith'
          description: 'The user name the license belongs to'
        product:
          type: string
          example: 'my-theme'
          description: 'The product the key is for'
        license:
          type: string
          example: 'jsmith'
          description: 'The license key'
        expiry:
          type: string
          format: datetime
          description: 'The date the license expires'

    product:
      type: object
      properties:
        name:
          type: string
          example: 'my-theme'
          description: 'The name of the product (theme or plugin).'
        description:
          type: string
          example: 'A description'
          description: 'A description of the product.'
        package_url:
          type: string
          example: 'my-theme'
          description: 'The download URL for the product. This is a URL to the get API action on the license manager server.'
        last_updated:
          type: string
          format: datetime
          description: 'The date the product was last updated'
        version:
          type: string
          description: 'Current version available on the server.'
        description_url:
          type: string
          description: 'A URL to a page that can be used for showing more information about the product.'
        tested:
          type: string
          description: 'The highest WordPress version on which the product has been tested (needed only for plugins).'
        banner_low:
          type: string
          description: 'Low resolution (regular) version of the product banner image (only needed for plugins).'
        banner_high:
          type: string
          description: 'High resolution (retina) version of the product banner image (only needed for plugins).'

    badRequest:
      type: object
      properties:
        type:
          type: string
          example: 'https://example.com/api/badRequest'
          description: 'A URI reference that identifies the problem type'
        title:
          type: string
          example: 'Title'
          description: 'A short, human-readable summary of the problem type'
        status:
          type: string
          example: '400'
          description: 'The HTTP status code'
        invalid-params:
          type: array
          items:
            type: object
            properties:
              name:
                type: string
              reason:
                type: string

    authentication:
      type: object
      properties:
        type:
          type: string
          example: 'https://example.com/api/authenication-error'
          description: 'A URI reference that identifies the problem type'
        title:
          type: string
          example: 'Title'
          description: 'A short, human-readable summary of the problem type'
        status:
          type: string
          example: '400'
          description: 'The HTTP status code'

    500s:
      type: object
      properties:
        type:
          type: string
          example: 'https://example.com/api/server-error'
          description: 'A URI reference that identifies the problem type'
        title:
          type: string
          example: 'Title'
          description: 'A short, human-readable summary of the problem type'
        status:
          type: string
          example: '400'
          description: 'The HTTP status code'

  parameters:
    licenseKey:
      name: key
      in: path
      description: License Key
      required: true
      schema:
        type: string
        format: uuid
    productName:
      name: name
      in: path
      description: Name of the product
      required: true
      schema:
        type: string

  responses:
    license:
      description: Requested operation was successful.
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/license"
    product:
      description: Product details
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/product"
    error:
      content:
        application/problem+json:
          schema:
            oneOf:
              - $ref: "#/components/schemas/badRequest"
              - $ref: "#/components/schemas/authentication"
              - $ref: "#/components/schemas/500s"

  securitySchemes:
    ApiKeyAuth:
      type: apiKey
      in: header
      name: X-API-KEY
      description: API Key set in API Gateway