<?php

/**
 * Base manager class for Adaptiff License Manager.
 *
 * @author    Adaptiff Designs <info@adaptiffdesigns.com.au>
 * @copyright 2020
 * @license   MIT
 *
 */

namespace Adaptiff\LicenseManager;

class ThemeManager extends BaseManager
{

    /**
     * The product id used for this product on the License Manager site.
     *
     * @var string The product id of the related product in the license manager.
     */
    protected $productId = '';

    /**
     * The name of the product using this class. Configured in the class's constructor.
     *
     * @var string The name of the product (plugin / theme) using this class.
     */
    protected $productName = '';

    /**
     * The text domain of the plugin or theme using this class.
     * Populated in the class's constructor.
     *
     * @var string  The text domain of the plugin / theme.
     */
    protected $textDomain = '';

    /**
     * @var string  The absolute path to the plugin's main file. Only applicable when using the
     *              class with a plugin.
     */
    protected $pluginFile = '';


    /**
     * Initializes the theme manager client.
     *
     * @param string $productId
     * @param string $productName
     * @param string $textDomain
     * @param string $api
     */
    public function __construct(
        string $productId,
        string $productName,
        string $textDomain,
        $api,
        $theme,
        $options
    ) {
        $this->productId   = $productId;
        $this->productName = $productName;
        $this->textDomain  = $textDomain;
        parent::__construct($api, $theme, $options);
    }


    /**
     * Gets the type of the manager.
     *
     * @return string
     */
    final public function getType()
    {
        return 'theme';
    }

    /**
     * @return string The theme version of the local installation.
     */
    final public function getLocalVersion()
    {
        $themeData = $this->getObject();
        return $themeData->Version;
    }
}
