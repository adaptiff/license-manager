<?php
/**
 * Base manager class for Adaptiff License Manager.
 *
 * @author    Adaptiff Designs <info@adaptiffdesigns.com.au>
 * @copyright 2020
 * @license   MIT
 *
 */

namespace Adaptiff\LicenseManager;

use GuzzleHttp\Client;

use function GuzzleHttp\json_decode;

abstract class BaseManager
{

    /**
     * The API endpoint. Configured through the class's constructor.
     *
     * @var GuzzelHttp\Client
     */
    private $api = '';

    /**
     * The API endpoint. Configured through the class's constructor.
     *
     * @var object
     */
    private $object = null;

    /**
     * The API endpoint. Configured through the class's constructor.
     *
     * @var array
     */
    private $options = [];

    /**
     * The product id (slug) used for this product on the License Manager site.
     * Configured through the class's constructor.
     *
     * @var string The product id of the related product in the license manager.
     */
    protected $productId;

    /**
     * The name of the product using this class. Configured in the class's constructor.
     *
     * @var string The name of the product (plugin / theme) using this class.
     */
    protected $productName;

    /**
     * The text domain of the plugin or theme using this class.
     * Populated in the class's constructor.
     *
     * @var string  The text domain of the plugin / theme.
     */
    protected $textDomain;

    /**
     * @var string  The absolute path to the plugin's main file. Only applicable when using the
     *              class with a plugin.
     */
    protected $pluginFile;


    /**
     * Initializes the license manager client.
     *
     * new Client(['base_uri' => $this->apiEndpoint, 'timeout'  => 2.0 ]);
     *
     * @param string $api
     *
     * @return void
     */
    public function __construct($api, $object, $options)
    {
        $this->api = $api;
        $this->object = $object;
        $this->options = $options;
    }


    /**
     * Gets the type of the manager.
     *
     * @return string
     */
    abstract public function getType();


    /**
     * @return string The theme/plugin version of the local installation.
     */
    abstract public function getLocalVersion();


    /**
     * Gets the object the manager is upgrading.
     *
     * Either wordpress theme or plugin.
     *
     * @return object
     */
    final public function getObject()
    {
        return $this->object;
    }


    /**
     * Gets the options set in the manager.
     *
     *
     * @return array
     */
    final public function getOptions()
    {
        return $this->options;
    }


    /**
     * Gets an option set in the manager.
     *
     * @param string $option The option to get.
     * @param mixed  $default The default value to return if no option is set.
     *
     * @return mixed
     */
    final public function getOption(string $option, $default = null)
    {
        if (isset($this->options[$option]) === true) {
            return $this->options[$option];
        }

        return $default;
    }


    /**
     * Undocumented function
     *
     * @param string $action
     * @param array $headers
     * @return void
     */
    final private function callApi(string $method, string $action, array $headers = [])
    {
        $result = null;
        try {
            $response     = $this->api->request($method, $action, ['headers' => $headers]);
            $responseBody = $response->getBody();
            $result       = json_decode($responseBody, true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            if ($e->hasResponse() === true) {
                return json_decode($e->getResponse()->getBody(), true);
            }
        }

        return $result;
    }


    /**
     * Calls the API to get the current license info.
     *
     * @return array
     */
    final public function getLicenseInfo()
    {
        if (!isset($this->options['email']) || !isset($this->options['license_key'])) {
            // User hasn't saved the license to settings yet. No use making the call.
            return false;
        }

        return $this->callApi('GET', '/license/' . $this->options['license_key'] . '/product/' . $this->productId);
    }


    /**
     * Checks the license manager to see if there is an update available for this theme.
     *
     * @return object|bool  If there is an update, returns the license information.
     *                      Otherwise returns false.
     */
    final public function isUpdateAvailable()
    {
        $licenseInfo = $this->getLicenseInfo();
        if (version_compare($licenseInfo['version'], $this->getLocalVersion(), '>')) {
            return $licenseInfo;
        }

        return false;
    }
}
