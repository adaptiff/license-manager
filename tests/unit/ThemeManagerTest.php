<?php

/**
 * Base manager class for Adaptiff License Manager.
 *
 * @author    Adaptiff Designs <info@adaptiffdesigns.com.au>
 * @copyright 2020
 * @license   MIT
 *
 */

namespace Adaptiff\LicenseManager\Tests\Unit;

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class ThemeManagerTests extends TestCase
{

    /**
     * Theme object
     *
     * @var object
     */
    private $theme;


    /**
     * Setup function.
     *
     * @return void
     */
    public function setUp(): void
    {
        // Create a mock and queue two responses.
        $mock = new MockHandler(
            [
                new Response(
                    200,
                    ['ContentType' => 'application/json'],
                    '{"name": "my-theme","description": "A description","package_url": "my-theme","last_updated": "string","version": "1.0.0","description_url": "string","tested": "string","banner_low": "string","banner_high": "string"}'
                ),
                new RequestException(
                    'Error Communicating with Server',
                    new Request('GET', 'license/key/product/name'),
                    new Response(
                        500,
                        ['ContentType' => 'application/json'],
                        '{"type": "https://example.com/api/server-error","title": "Title","status": "500"}'
                    )
                ),
                new Response(
                    200,
                    ['ContentType' => 'application/json'],
                    '{"name": "my-theme","description": "A description","package_url": "my-theme","last_updated": "string","version": "2.0.0","description_url": "string","tested": "string","banner_low": "string","banner_high": "string"}'
                ),
                new Response(
                    200,
                    ['ContentType' => 'application/json'],
                    '{"name": "my-theme","description": "A description","package_url": "my-theme","last_updated": "string","version": "1.0.0","description_url": "string","tested": "string","banner_low": "string","banner_high": "string"}'
                ),
            ]
        );

        $handlerStack = HandlerStack::create($mock);
        $this->client = new Client(['handler' => $handlerStack]);

        $this->theme = new \stdClass();
        $this->theme->Version = '1.0.0';

        $this->themeManager = new \Adaptiff\LicenseManager\ThemeManager(
            'test-theme',
            'Test Theme',
            'adaptiff',
            $this->client,
            $this->theme,
            [
                'email' => 'jsmith@example.com',
                'license_key' => 'key',
            ]
        );
    }


    /**
     * Tear down function.
     *
     * @return void
     */
    public function tearDown(): void
    {
        $this->client       = null;
        $this->themeManager = null;
    }


    /**
     * Test ThemeManage->getType()
     *
     * @return void
     */
    public function testGetType()
    {
        $this->assertEquals('theme', $this->themeManager->getType(), 'Expected theme type');
    }


    /**
     * Test ThemeManage->getOptions()
     *
     * @return void
     */
    public function testGetOptions()
    {
        $options = $this->themeManager->getOptions();
        $this->assertIsArray($options);
        $this->assertEquals('jsmith@example.com', $options['email']);
        $this->assertEquals('key', $options['license_key']);
    }

    /**
     * Test ThemeManage->getOption()
     *
     * @return void
     */
    public function testGetOption()
    {
        $option = $this->themeManager->getOption('email');
        $this->assertEquals('jsmith@example.com', $option);

        $option = $this->themeManager->getOption('email1', null);
        $this->assertNull($option);

        $option = $this->themeManager->getOption('email1', 'defaultValue');
        $this->assertEquals('defaultValue', $option);
    }


    /**
     * Test ThemeManage->getLocalVersion()
     *
     * @return void
     */
    public function testGetLocalVersion()
    {
        $version = $this->themeManager->getLocalVersion();
        $this->assertIsString($version, 'Version should be a string');
    }


    /**
     * Test ThemeManage->getLicenseInfo()
     *
     * @return void
     */
    public function testGetLicenseInfo()
    {
        // Valid manager
        $response = $this->themeManager->getLicenseInfo();
        $this->assertIsArray($response);
        $this->assertArrayHasKey('version', $response, 'Missing version key');

        $response = $this->themeManager->getLicenseInfo();
        $this->assertIsArray($response);
        $this->assertArrayHasKey('type', $response, 'Missing type key');
        $this->assertArrayHasKey('title', $response, 'Missing title key');
        $this->assertArrayHasKey('status', $response, 'Missing status key');

        // Missing license key.
        $themeManager1 = new \Adaptiff\LicenseManager\ThemeManager(
            'test-theme',
            'Test Theme',
            'adaptiff',
            $this->client,
            $this->theme,
            [
                'email' => 'jsmith@example.com',
            ]
        );
        $response = $themeManager1->getLicenseInfo();
        $this->assertFalse($response, 'No license info should be returned');

        // No email or license key, but some other option set.
        $themeManager1 = new \Adaptiff\LicenseManager\ThemeManager(
            'test-theme',
            'Test Theme',
            'adaptiff',
            $this->client,
            $this->theme,
            [
                'some-key' => 'random string',
            ]
        );
        $response = $themeManager1->getLicenseInfo();
        $this->assertFalse($response, 'No license info should be returned');

        $themeManager1 = new \Adaptiff\LicenseManager\ThemeManager(
            'test-theme',
            'Test Theme',
            'adaptiff',
            $this->client,
            $this->theme,
            [
                'random string',
            ]
        );
        $response = $themeManager1->getLicenseInfo();
        $this->assertFalse($response, 'No license info should be returned');
    }


    /**
     * Test ThemeManage->getLocalVersion()
     *
     * @return void
     */
    public function testIsUpdateAvailable()
    {
        $mock = new MockHandler(
            [
                new Response(
                    200,
                    ['ContentType' => 'application/json'],
                    '{"name": "my-theme","description": "A description","package_url": "my-theme","last_updated": "string","version": "2.0.0","description_url": "string","tested": "string","banner_low": "string","banner_high": "string"}'
                ),
                new Response(
                    200,
                    ['ContentType' => 'application/json'],
                    '{"name": "my-theme","description": "A description","package_url": "my-theme","last_updated": "string","version": "1.0.0","description_url": "string","tested": "string","banner_low": "string","banner_high": "string"}'
                ),
            ]
        );

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);


        $themeManager = new \Adaptiff\LicenseManager\ThemeManager(
            'test-theme',
            'Test Theme',
            'adaptiff',
            $client,
            $this->theme,
            [
                'email' => 'jsmith@example.com',
                'license_key' => 'key',
            ]
        );

        $update = $themeManager->isUpdateAvailable();
        $this->assertIsArray($update);

        $update = $themeManager->isUpdateAvailable();
        $this->assertFalse($update);
    }
}
